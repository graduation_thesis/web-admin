import DecisionService from "../../services/decision";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    decision_id: "",
    decision_file: "",
    sign_date: "",
    received_date: "",
    description: "",
    image: ""
  }
};

const DecisionAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        decision_id: state.basicInfo.decision_id,
        decision_file: state.basicInfo.decision_file,
        sign_date: state.basicInfo.sign_date,
        received_date: state.basicInfo.received_date,
        description: state.basicInfo.description,
        image: state.basicInfo.image
      };
      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        DecisionService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.decision_id = basicInfo.decision_id;
      state.basicInfo.decision_file = basicInfo.decision_file;
      state.basicInfo.sign_date = basicInfo.sign_date;
      state.basicInfo.received_date = basicInfo.received_date;
      state.basicInfo.description = basicInfo.description;
      state.basicInfo.image = basicInfo.image;
    },

    reset(state) {
      state.basicInfo.name = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default DecisionAddModule;
