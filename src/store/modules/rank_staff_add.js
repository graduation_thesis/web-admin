import RankStaffService from "../../services/rank_staff.js";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    rank_staff_id: "",
    name: ""
  }
};

const RankStaffAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        rank_staff_id: state.basicInfo.rank_staff_id,
        name: state.basicInfo.name
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        RankStaffService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);
            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.rank_staff_id = basicInfo.rank_staff_id;
      state.basicInfo.name = basicInfo.name;
    },

    reset(state) {
      state.basicInfo.name = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default RankStaffAddModule;
