import UnitService from "../../services/unit";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    school_id: "",
    unit_id: "",
    name: "",
    build_date: ""
  }
};

const UnitAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        school_id: state.basicInfo.school_id,
        unit_id: state.basicInfo.unit_id,
        name: state.basicInfo.name,
        build_date: state.basicInfo.build_date
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        UnitService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.school_id = basicInfo.school_id;
      state.basicInfo.unit_id = basicInfo.unit_id;
      state.basicInfo.name = basicInfo.name;
      state.basicInfo.build_date = basicInfo.build_date;
    },

    reset(state) {
      state.basicInfo.name = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default UnitAddModule;
