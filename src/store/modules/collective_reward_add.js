import CollectiveRewardService from "../../services/collective_reward";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    school_id: "",
    unit_id: "",
    forms_of_reward_id: "",
    decision_agency_id: "",
    decision_id: "",
    reward_date: ""
  }
};

const CollectiveRewardAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        school_id: state.basicInfo.school_id,
        unit_id: state.basicInfo.unit_id,
        forms_of_reward_id: state.basicInfo.forms_of_reward_id,
        decision_agency_id: state.basicInfo.decision_agency_id,
        decision_id: state.basicInfo.decision_id,
        reward_date: state.basicInfo.reward_date
      };
      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        CollectiveRewardService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.school_id = basicInfo.school_id;
      state.basicInfo.unit_id = basicInfo.unit_id;
      state.basicInfo.forms_of_reward_id = basicInfo.forms_of_reward_id;
      state.basicInfo.decision_agency_id = basicInfo.decision_agency_id;
      state.basicInfo.decision_id = basicInfo.decision_id;
      state.basicInfo.reward_date = basicInfo.reward_date;
    },

    reset(state) {
      state.basicInfo.unit_id = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default CollectiveRewardAddModule;
