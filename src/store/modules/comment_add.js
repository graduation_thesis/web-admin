import CommentService from "../../services/comment";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    user_id: "",
    content_new: "",
    feedback: "",
    status: ""
  }
};

const CommentAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        user_id: state.basicInfo.user_id,
        content_new: state.basicInfo.content_new,
        feedback: state.basicInfo.feedback,
        status: state.basicInfo.status
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        CommentService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.user_id = basicInfo.user_id;
      state.basicInfo.content_new = basicInfo.content_new;
      state.basicInfo.feedback = basicInfo.feedback;
      state.basicInfo.status = basicInfo.status;
    },

    reset(state) {
      state.basicInfo.content = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default CommentAddModule;
