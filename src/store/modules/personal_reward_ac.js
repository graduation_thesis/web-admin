import StaffService from "../../services/staff";

const StaffAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    staffOptions: []
  },
  actions: {
    loadStaffData({ commit }) {
      commit("onloadStaffData");
      return new Promise((resolve, reject) => {
        StaffService.getAll()
          .then(resp => {
            commit("onProcessingStaffData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingStaffDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadStaffData(state) {
      state.isLoading = true;
    },
    onProcessingStaffData(state, data) {
      state.isLoading = false;

      state.staffOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.staffOptions.push({
          value: data[i]["id"],
          text: data[i]["full_name"]
        });
      }
    },
    onProcessingStaffDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default StaffAcModule;
