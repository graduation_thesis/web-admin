import StaffService from "../../services/staff";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    school_id: "",
    unit_id: "",
    team_id: "",
    full_name: "",
    birthday: "",
    sex: "",
    code_number: "",
    type_staff_id: "",
    rank_staff_id: "",
    position_staff_id: ""
  }
};

const StaffAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        school_id: state.basicInfo.school_id,
        unit_id: state.basicInfo.unit_id,
        team_id: state.basicInfo.team_id,
        full_name: state.basicInfo.full_name,
        birthday: state.basicInfo.birthday,
        sex: state.basicInfo.sex,
        code_number: state.basicInfo.code_number,
        type_staff_id: state.basicInfo.type_staff_id,
        rank_staff_id: state.basicInfo.rank_staff_id,
        position_staff_id: state.basicInfo.position_staff_id
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        StaffService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);
            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.school_id = basicInfo.school_id;
      state.basicInfo.unit_id = basicInfo.unit_id;
      state.basicInfo.team_id = basicInfo.team_id;
      state.basicInfo.full_name = basicInfo.full_name;
      state.basicInfo.birthday = basicInfo.birthday;
      state.basicInfo.sex = basicInfo.sex;
      state.basicInfo.code_number = basicInfo.code_number;
      state.basicInfo.type_staff_id = basicInfo.type_staff_id;
      state.basicInfo.rank_staff_id = basicInfo.rank_staff_id;
      state.basicInfo.position_staff_id = basicInfo.position_staff_id;
    },

    reset(state) {
      state.basicInfo.full_name = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default StaffAddModule;
