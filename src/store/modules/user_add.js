import UserService from "../../services/user";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    code_number: "",
    school_id: "",
    unit_id: "",
    username: "",
    password: "",
    role: ""
  }
};

const UserAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        code_number: state.basicInfo.code_number,
        school_id: state.basicInfo.school_id,
        unit_id: state.basicInfo.unit_id,
        username: state.basicInfo.username,
        password: state.basicInfo.password,
        role: state.basicInfo.role
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        UserService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.code_number = basicInfo.code_number;
      state.basicInfo.school_id = basicInfo.school_id;
      state.basicInfo.unit_id = basicInfo.unit_id;
      state.basicInfo.username = basicInfo.username;
      state.basicInfo.password = basicInfo.password;
      state.basicInfo.role = basicInfo.role;
    },

    reset(state) {
      state.basicInfo.username = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default UserAddModule;
