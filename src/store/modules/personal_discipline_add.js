import PersonalDisciplineService from "../../services/personal_discipline";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    code_number: "",
    forms_of_discipline_id: "",
    decision_agency_id: "",
    decision_id: "",
    discipline_date: ""
  }
};

const PersonalDisciplineAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        code_number: state.basicInfo.code_number,
        forms_of_discipline_id: state.basicInfo.forms_of_discipline_id,
        decision_agency_id: state.basicInfo.decision_agency_id,
        decision_id: state.basicInfo.decision_id,
        discipline_date: state.basicInfo.discipline_date
      };
      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        PersonalDisciplineService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.code_number = basicInfo.code_number;
      state.basicInfo.forms_of_discipline_id = basicInfo.forms_of_discipline_id;
      state.basicInfo.decision_agency_id = basicInfo.decision_agency_id;
      state.basicInfo.decision_id = basicInfo.decision_id;
      state.basicInfo.discipline_date = basicInfo.discipline_date;
    },

    reset(state) {
      state.basicInfo.code_number = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default PersonalDisciplineAddModule;
