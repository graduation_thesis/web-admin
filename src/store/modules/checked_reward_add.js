import CheckedRewardService from "../../services/checked_reward";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    user_id: "",
    code_number: "",
    school_id: "",
    unit_id: "",
    rank_staff_id: "",
    personal_title_id: "",
    reward_date: ""
  }
};

const CheckedRewardAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        user_id: state.basicInfo.user_id,
        code_number: state.basicInfo.code_number,
        school_id: state.basicInfo.school_id,
        unit_id: state.basicInfo.unit_id,
        rank_staff_id: state.basicInfo.rank_staff_id,
        personal_title_id: state.basicInfo.personal_title_id,
        reward_date: state.basicInfo.reward_date
      };
      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        CheckedRewardService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.user_id = basicInfo.user_id;
      state.basicInfo.code_number = basicInfo.code_number;
      state.basicInfo.school_id = basicInfo.school_id;
      state.basicInfo.unit_id = basicInfo.unit_id;
      state.basicInfo.rank_staff_id = basicInfo.rank_staff_id;
      state.basicInfo.personal_title_id = basicInfo.personal_title_id;
      state.basicInfo.reward_date = basicInfo.reward_date;
    },

    reset(state) {
      state.basicInfo.code_number = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default CheckedRewardAddModule;
