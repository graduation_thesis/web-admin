import FormsOfRewardService from "../../../services/forms_of_reward";

const FormsOfRewardAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    formsOfRewardOptions: []
  },
  actions: {
    loadFormsOfRewardData({ commit }) {
      commit("onloadFormsOfRewardData");
      return new Promise((resolve, reject) => {
        FormsOfRewardService.getAll()
          .then(resp => {
            commit("onProcessingFormsOfRewardData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingFormsOfRewardDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadFormsOfRewardData(state) {
      state.isLoading = true;
    },
    onProcessingFormsOfRewardData(state, data) {
      state.isLoading = false;

      state.formsOfRewardOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.formsOfRewardOptions.push({
          value: data[i]["forms_of_reward_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingFormsOfRewardDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default FormsOfRewardAcModule;
