import RankStaffService from "../../../services/rank_staff";

const RankStaffAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    rankStaffOptions: []
  },
  actions: {
    loadRankStaffData({ commit }) {
      commit("onloadRankStaffData");
      return new Promise((resolve, reject) => {
        RankStaffService.getAll()
          .then(resp => {
            commit("onProcessingRankStaffData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingRankStaffDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadRankStaffData(state) {
      state.isLoading = true;
    },
    onProcessingRankStaffData(state, data) {
      state.isLoading = false;

      state.rankStaffOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.rankStaffOptions.push({
          value: data[i]["rank_staff_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingRankStaffDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default RankStaffAcModule;
