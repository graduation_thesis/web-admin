import PersonalTitleService from "../../../services/personal_title";

const PersonalTitleAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    personalTitleOptions: []
  },
  actions: {
    loadPersonalTitleData({ commit }) {
      commit("onloadPersonalTitleData");
      return new Promise((resolve, reject) => {
        PersonalTitleService.getAll()
          .then(resp => {
            commit("onProcessingPersonalTitleData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingPersonalTitleDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadPersonalTitleData(state) {
      state.isLoading = true;
    },
    onProcessingPersonalTitleData(state, data) {
      state.isLoading = false;

      state.personalTitleOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.personalTitleOptions.push({
          value: data[i]["personal_title_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingPersonalTitleDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default PersonalTitleAcModule;
