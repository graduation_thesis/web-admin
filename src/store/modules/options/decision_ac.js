import DecisionService from "../../../services/decision";

const DecisionAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    decisionOptions: []
  },
  actions: {
    loadDecisionData({ commit }) {
      commit("onloadDecisionData");
      return new Promise((resolve, reject) => {
        DecisionService.getAll()
          .then(resp => {
            commit("onProcessingDecisionData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingDecisionDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadDecisionData(state) {
      state.isLoading = true;
    },
    onProcessingDecisionData(state, data) {
      state.isLoading = false;

      state.decisionOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.decisionOptions.push({
          value: data[i]["decision_id"],
          text: data[i]["decision_id"]
        });
      }
    },
    onProcessingDecisionDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default DecisionAcModule;
