import FormsRiskService from "../../../services/forms_risk";

const FormsRiskAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    formsRiskOptions: []
  },
  actions: {
    loadFormsRiskData({ commit }) {
      commit("onloadFormsRiskData");
      return new Promise((resolve, reject) => {
        FormsRiskService.getAll()
          .then(resp => {
            commit("onProcessingFormsRiskData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingFormsRiskDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadFormsRiskData(state) {
      state.isLoading = true;
    },
    onProcessingFormsRiskData(state, data) {
      state.isLoading = false;

      state.formsRiskOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.formsRiskOptions.push({
          value: data[i]["forms_risk_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingFormsOfRewardDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default FormsRiskAcModule;
