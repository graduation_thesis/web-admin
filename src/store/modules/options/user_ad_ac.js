import UserAcService from "../../../services/user_ac.js";

const UserAdAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    schoolOptions: [],
    unitOptions: [],
    staffOptions: []
  },
  actions: {
    loadSchoolData({ commit }) {
      commit("onLoadSchoolData");
      return new Promise((resolve, reject) => {
        UserAcService.getSchools()
          .then(resp => {
            commit("onProcessingSchoolData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingSchoolDataFailure");
            reject(err);
          });
      });
    },

    changeSchool({ commit }, schoolId) {
      return new Promise((resolve, reject) => {
        UserAcService.getUnits(schoolId)
          .then(resp => {
            commit("onProcessingUnitData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    loadUnitData({ commit }) {
      commit("onLoadSchoolData");
      return new Promise((resolve, reject) => {
        UserAcService.getUnitAll()
          .then(resp => {
            commit("onProcessingUnitData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingUnitDataFailure");
            reject(err);
          });
      });
    },

    changeUnit({ commit }, unitId) {
      return new Promise((resolve, reject) => {
        UserAcService.getStaffs(unitId)
          .then(resp => {
            commit("onProcessingStaffData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadSchoolData(state) {
      state.isLoading = true;
    },
    onProcessingSchoolData(state, data) {
      state.isLoading = false;

      state.schoolOptions = [
        { value: 0, text: "-- Chọn đơn vị trên cơ sở --" }
      ];
      state.unitOptions = [{ value: 0, text: "-- Chọn đơn vị cơ sở --" }];
      state.staffOptions = [{ value: 0, text: "-- Chọn cán bộ --" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.schoolOptions.push({
          value: data[i]["school_id"],
          text: data[i]["name"]
        });
      }
    },

    onProcessingSchoolDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingUnitDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingUnitData(state, data) {
      state.units = data;

      state.unitOptions = [{ value: 0, text: "-- Chọn đơn vị cơ sở --" }];
      state.staffOptions = [{ value: 0, text: "-- Chọn cán bộ --" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.unitOptions.push({
          value: data[i]["unit_id"],
          text: data[i]["name"]
        });
      }
    },

    onProcessingStaffData(state, data) {
      state.staffs = data;

      state.staffOptions = [{ value: 0, text: "-- Chọn cán bộ --" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.staffOptions.push({
          value: data[i]["code_number"],
          text:
            data[i]["full_name"] + "==>" + "[" + data[i]["ranks"]["name"] + "]"
        });
      }
    }
  }
};

export default UserAdAcModule;
