import TypeStaffService from "../../../services/type_staff";

const TypeStaffAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    typeStaffOptions: []
  },
  actions: {
    loadTypeStaffData({ commit }) {
      commit("onloadTypeStaffData");
      return new Promise((resolve, reject) => {
        TypeStaffService.getAll()
          .then(resp => {
            commit("onProcessingTypeStaffData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingTypeStaffDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadTypeStaffData(state) {
      state.isLoading = true;
    },
    onProcessingTypeStaffData(state, data) {
      state.isLoading = false;

      state.typeStaffOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.typeStaffOptions.push({
          value: data[i]["type_staff_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingTypeStaffDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default TypeStaffAcModule;
