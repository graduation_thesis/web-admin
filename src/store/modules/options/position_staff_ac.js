import PositionStaffService from "../../../services/position_staff";

const PositionStaffAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    positionStaffOptions: []
  },
  actions: {
    loadPositionStaffData({ commit }) {
      commit("onloadPositionStaffData");
      return new Promise((resolve, reject) => {
        PositionStaffService.getAll()
          .then(resp => {
            commit("onProcessingPositionStaffData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingPositionStaffDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadPositionStaffData(state) {
      state.isLoading = true;
    },
    onProcessingPositionStaffData(state, data) {
      state.isLoading = false;

      state.positionStaffOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.positionStaffOptions.push({
          value: data[i]["position_staff_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingPositionStaffDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default PositionStaffAcModule;
