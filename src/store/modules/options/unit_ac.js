import UnitService from "../../../services/unit";

const UnitAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    unitOptions: []
  },
  actions: {
    loadUnitData({ commit }) {
      commit("onloadUnitData");
      return new Promise((resolve, reject) => {
        UnitService.getAll()
          .then(resp => {
            commit("onProcessingUnitData", resp.data.data);
            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingUnitDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadUnitData(state) {
      state.isLoading = true;
    },
    onProcessingUnitData(state, data) {
      state.isLoading = false;

      state.unitOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.unitOptions.push({
          id: data[i]["id"],
          value: data[i]["unit_id"],
          text: data[i]["name"],
          valueSchoolId: data[i]["school_id"],
          txtSchool: data[i]["schools"].name
        });
      }
    },
    onProcessingUnitDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default UnitAcModule;
