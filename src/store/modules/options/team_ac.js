import TeamAcService from "../../../services/team_ac.js";

const TeamAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    schoolOptions: [],
    unitOptions: []
  },
  actions: {
    loadSchoolData({ commit }) {
      commit("onLoadSchoolData");
      return new Promise((resolve, reject) => {
        TeamAcService.getSchools()
          .then(resp => {
            commit("onProcessingSchoolData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingSchoolDataFailure");
            reject(err);
          });
      });
    },

    changeSchool({ commit }, schoolID) {
      return new Promise((resolve, reject) => {
        TeamAcService.getUnits(schoolID)
          .then(resp => {
            commit("onProcessingUnitData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    changeUnit({ commit }, unitId) {
      return new Promise((resolve, reject) => {
        TeamAcService.getUnitBundle(unitId)
          .then(resp => {
            commit("onProcessingUnitBundleData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadSchoolData(state) {
      state.isLoading = true;
    },
    onProcessingSchoolData(state, data) {
      state.isLoading = false;

      state.schoolOptions = [
        { value: 0, text: "-- Chọn đơn vị trên cơ sở --" }
      ];
      state.unitOptions = [{ value: 0, text: "-- Chọn đơn vị cơ sở --" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.schoolOptions.push({
          value: data[i]["school_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingSchoolDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingUnitData(state, data) {
      state.units = data;

      state.unitOptions = [{ value: 0, text: "-- Chọn đơn vị cơ sở --" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.unitOptions.push({
          value: data[i]["unit_id"],
          text: data[i]["name"]
        });
      }
    }
  }
};

export default TeamAcModule;
