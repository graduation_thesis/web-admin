import FormsOfDisciplineService from "../../../services/forms_of_discipline";

const FormsOfDisciplineAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    formsOfDisciplineOptions: []
  },
  actions: {
    loadFormsOfDisciplineData({ commit }) {
      commit("onloadFormsOfDisciplineData");
      return new Promise((resolve, reject) => {
        FormsOfDisciplineService.getAll()
          .then(resp => {
            commit("onProcessingFormsOfDisciplineData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingFormsOfDisciplineDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadFormsOfDisciplineData(state) {
      state.isLoading = true;
    },
    onProcessingFormsOfDisciplineData(state, data) {
      state.isLoading = false;

      state.formsOfDisciplineOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.formsOfDisciplineOptions.push({
          value: data[i]["forms_of_discipline_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingFormsOfDisciplineDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default FormsOfDisciplineAcModule;
