import CollectiveTitleService from "../../../services/collective_title";

const PersonalTitleAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    collectiveTitleOptions: []
  },
  actions: {
    loadCollectiveTitleData({ commit }) {
      commit("onloadCollectiveTitleData");
      return new Promise((resolve, reject) => {
        CollectiveTitleService.getAll()
          .then(resp => {
            commit("onProcessingCollectiveTitleData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingCollectiveTitleDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadCollectiveTitleData(state) {
      state.isLoading = true;
    },
    onProcessingCollectiveTitleData(state, data) {
      state.isLoading = false;

      state.collectiveTitleOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.collectiveTitleOptions.push({
          value: data[i]["collective_title_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingCollectiveTitleDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default PersonalTitleAcModule;
