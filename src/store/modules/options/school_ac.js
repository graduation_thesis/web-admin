import SchoolService from "../../../services/school";

const SchoolAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    schoolOptions: []
  },
  actions: {
    loadSchoolData({ commit }) {
      commit("onloadSchoolData");
      return new Promise((resolve, reject) => {
        SchoolService.getAll()
          .then(resp => {
            commit("onProcessingSchoolData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingSchoolDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadSchoolData(state) {
      state.isLoading = true;
    },
    onProcessingSchoolData(state, data) {
      state.isLoading = false;

      state.schoolOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.schoolOptions.push({
          value: data[i]["school_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingSchoolDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default SchoolAcModule;
