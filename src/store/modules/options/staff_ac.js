import StaffService from "../../../services/staff";

const StaffAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    staffOptions: []
  },
  actions: {
    loadStaffData({ commit }) {
      commit("onloadStaffData");
      return new Promise((resolve, reject) => {
        StaffService.getAll()
          .then(resp => {
            commit("onProcessingStaffData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingStaffDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadStaffData(state) {
      state.isLoading = true;
    },
    onProcessingStaffData(state, data) {
      state.isLoading = false;

      state.staffOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.staffOptions.push({
          id: data[i]["id"],
          value: data[i]["code_number"],
          text: data[i]["full_name"],
          txtUnit: data[i]["units"].name,
          curentYear: data[i]["created_at"],
          valueCodeNumber: data[i]["code_number"],
          valueSchoolId: data[i]["school_id"],
          valueUnitId: data[i]["unit_id"],
          valueTeamId: data[i]["team_id"],
          valueRankId: data[i]["rank_staff_id"],

          // Lấy dữ liệu wa controller để xử lý
          txtFullName: data[i]["full_name"],
          txtBirthday: data[i]["birthday"],
          txtSex: data[i]["sex"],
          valueTypeId: data[i]["type_staff_id"],
          valuePositionId: data[i]["position_staff_id"]
        });
      }
    },
    onProcessingStaffDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default StaffAcModule;
