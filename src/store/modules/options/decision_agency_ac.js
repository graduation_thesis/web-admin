import DecisionAgencyService from "../../../services/decision_agency";

const DecisionAgencyAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    decisionAgencyOptions: []
  },
  actions: {
    loadDecisionAgencyData({ commit }) {
      commit("onloadDecisionAgencyData");
      return new Promise((resolve, reject) => {
        DecisionAgencyService.getAll()
          .then(resp => {
            commit("onProcessingDecisionAgencyData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingDecisionAgencyDataFailure");
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onloadDecisionAgencyData(state) {
      state.isLoading = true;
    },
    onProcessingDecisionAgencyData(state, data) {
      state.isLoading = false;

      state.decisionAgencyOptions = [];

      for (let i = 0, l = data.length; i < l; i++) {
        state.decisionAgencyOptions.push({
          value: data[i]["decision_agency_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingDecisionAgencyDataFailure(state) {
      state.isLoading = false;
    }
  }
};

export default DecisionAgencyAcModule;
