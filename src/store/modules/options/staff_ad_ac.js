import StaffAcService from "../../../services/staff_ac.js";

const StaffAdAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    schoolOptions: [],
    unitOptions: [],
    teamOptions: []
  },
  actions: {
    loadSchoolData({ commit }) {
      commit("onLoadSchoolData");
      return new Promise((resolve, reject) => {
        StaffAcService.getSchools()
          .then(resp => {
            commit("onProcessingSchoolData", resp.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingSchoolDataFailure");
            reject(err);
          });
      });
    },

    changeSchool({ commit }, schoolId) {
      return new Promise((resolve, reject) => {
        StaffAcService.getUnits(schoolId)
          .then(resp => {
            commit("onProcessingUnitData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    loadUnitData({ commit }) {
      commit("onLoadSchoolData");
      return new Promise((resolve, reject) => {
        StaffAcService.getUnitAll()
          .then(resp => {
            commit("onProcessingUnitData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingUnitDataFailure");
            reject(err);
          });
      });
    },

    changeUnit({ commit }, unitId) {
      return new Promise((resolve, reject) => {
        StaffAcService.getTeams(unitId)
          .then(resp => {
            commit("onProcessingTeamData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadSchoolData(state) {
      state.isLoading = true;
    },
    onProcessingSchoolData(state, data) {
      state.isLoading = false;

      state.schoolOptions = [
        { value: 0, text: "-- Chọn đơn vị trên cơ sở --" }
      ];
      state.unitOptions = [{ value: 0, text: "-- Chọn đơn vị cơ sở --" }];
      state.teamOptions = [{ value: null, text: "-- Chọn đội, đồn, tổ --" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.schoolOptions.push({
          value: data[i]["school_id"],
          text: data[i]["name"]
        });
      }
    },

    onProcessingSchoolDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingUnitDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingUnitData(state, data) {
      state.units = data;

      state.unitOptions = [{ value: 0, text: "-- Chọn đơn vị cơ sở --" }];
      state.teamOptions = [{ value: null, text: "-- Chọn đội, đồn, tổ --" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.unitOptions.push({
          value: data[i]["unit_id"],
          text: data[i]["name"]
        });
      }
    },

    onProcessingTeamData(state, data) {
      state.teams = data;

      state.teamOptions = [{ value: null, text: "-- Chọn đội, đồn, tổ --" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.teamOptions.push({
          value: data[i]["id"],
          text: data[i]["name"]
        });
      }
    }
  }
};

export default StaffAdAcModule;
