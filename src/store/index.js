import Vue from "vue";
import Vuex from "vuex";

import UserAddModule from "./modules/user_add.js";
import UnitAddModule from "./modules/unit_add.js";
// import UnitAcModule from './modules/staff_ac.js'

import StaffAddModule from "./modules/staff_add.js";

import TypeStaffAddModule from "./modules/type_staff_add.js";

import RankStaffAddModule from "./modules/rank_staff_add.js";

import PositionStaffAddModule from "./modules/position_staff_add.js";

import FormsOfRewardAddModule from "./modules/forms_of_reward_add.js";
import FormsOfDisciplineAddModule from "./modules/forms_of_discipline_add.js";

// Hình thức rủi ro
import FormsRiskAddModule from "./modules/forms_risk_add.js";

// Kỷ luật cá nhân
import DisciplineAddModule from "./modules/discipline_add.js";

// Khen thưởng cá nhân
import PersonalRewardAddModule from "./modules/personal_reward_add.js";
import StaffAcAddModule from "./modules/options/staff_ac.js";
import PersonalTitleAcAddModule from "./modules/options/personal_title_ac.js";
import UnitAcAddModule from "./modules/options/unit_ac.js";

// Khen thưởng cá nhân theo ID
import PersonalAchievementsAddModule from "./modules/personal_achievements_add.js";

// Kỷ luật theo ID
import PersonalDisciplineAddModule from "./modules/personal_discipline_add.js";

// Rủi ro cá nhân
import RiskDutyAddModule from "./modules/risk_duty_add.js";

// Khen thưởng tập thể
import CollectiveRewardAddModule from "./modules/collective_reward_add.js";

// Đăng kí thi đua cá nhân đã qua kiểm duyệt
import CheckedRewardAddModule from "./modules/checked_reward_add.js";
// Đăng kí thi đua đơn vị đã qua kiểm duyệt
import CheckedCollectiveRewardAddModule from "./modules/checked_collective_reward_add.js";

// Kết quả thi đua cá nhân
import PersonalRewardResultAddModule from "./modules/personal_reward_result_add.js";
// Kết quả thi đua đơn vị
import CollectiveRewardResultAddModule from "./modules/collective_reward_result_add.js";

// Các form hình thức
import FormsOfRewardAcAddModule from "./modules/options/forms_of_reward_ac.js";
import FormsRiskAcModule from "./modules/options/forms_risk_ac.js";
import FormsOfDisciplineAcAddModule from "./modules/options/forms_of_discipline_ac.js";

// Danh hiệu cá nhân
import PersonalTitleAddModule from "./modules/personal_title_add.js";

// Danh hiệu tập thể
import CollectiveTitleAddModule from "./modules/collective_title_add.js";
import CollectiveTitleAcAddModule from "./modules/options/collective_title_ac.js";

// Các đơn vị (trên - giữa - dưới)
import SchoolAddModule from "./modules/school_add.js";
import SchoolAcModule from "./modules/options/school_ac";
import TeamAddModule from "./modules/team_add";
import TeamAcModule from "./modules/options/team_ac";

// Liên quan cán bộ
import StaffAdAcModule from "./modules/options/staff_ad_ac";
import UserAdAcModule from "./modules/options/user_ad_ac";
import RankStaffAcModule from "./modules/options/rank_staff_ac";
import PositionStaffAcModule from "./modules/options/position_staff_ac";
import TypeStaffAcModule from "./modules/options/type_staff_ac";
import DecisionAgencyAddModule from "./modules/decision_agency_add";
import DecisionAgencyAcModule from "./modules/options/decision_agency_ac";

import DecisionAddModule from "./modules/decision_add";
import DecisionAcModule from "./modules/options/decision_ac";

// Tài liệu
import DocumentAddModule from "./modules/document_add";

// Auth
import AuthModule from "./modules/auth.js";

// Ý kiến phản hồi
import CommentAddModule from "./modules/comment_add.js";

// Quá trình công tác cá nhân
import ChangePersonalAddModule from "./modules/change_personal_add.js";
// Quá trình công tác đơn vị
import ChangeCollectiveAddModule from "./modules/change_collective_add.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth: AuthModule,

    comment_add: CommentAddModule,

    user_add: UserAddModule,
    unit_add: UnitAddModule,
    staff_add: StaffAddModule,
    type_staff_add: TypeStaffAddModule,
    rank_staff_add: RankStaffAddModule,
    position_staff_add: PositionStaffAddModule,
    // staff_ac: UnitAcModule,

    forms_of_reward_add: FormsOfRewardAddModule,
    forms_of_discipline_add: FormsOfDisciplineAddModule,

    forms_risk_add: FormsRiskAddModule,

    discipline_add: DisciplineAddModule,

    checked_reward_add: CheckedRewardAddModule,
    checked_collective_reward_add: CheckedCollectiveRewardAddModule,

    personal_reward_result_add: PersonalRewardResultAddModule,
    collective_reward_result_add: CollectiveRewardResultAddModule,

    personal_reward_add: PersonalRewardAddModule,
    risk_duty_add: RiskDutyAddModule,
    collective_reward_add: CollectiveRewardAddModule,

    // Khen thưởng theo ID
    personal_achievements_add: PersonalAchievementsAddModule,
    // Kỷ luật theo ID
    personal_discipline_add: PersonalDisciplineAddModule,

    staffs_ac: StaffAcAddModule,

    forms_of_reward_ac: FormsOfRewardAcAddModule,
    forms_risk_ac: FormsRiskAcModule,
    forms_of_discipline_ac: FormsOfDisciplineAcAddModule,

    personal_title_ac: PersonalTitleAcAddModule,

    personal_title_add: PersonalTitleAddModule,
    collective_title_add: CollectiveTitleAddModule,
    collective_title_ac: CollectiveTitleAcAddModule,

    unit_ac: UnitAcAddModule,

    school_add: SchoolAddModule,
    school_ac: SchoolAcModule,

    team_add: TeamAddModule,

    team_ac: TeamAcModule,

    staff_ad_ac: StaffAdAcModule,
    user_ad_ac: UserAdAcModule,

    rank_staff_ac: RankStaffAcModule,

    position_staff_ac: PositionStaffAcModule,

    type_staff_ac: TypeStaffAcModule,

    decision_agency_add: DecisionAgencyAddModule,
    decision_agency_ac: DecisionAgencyAcModule,

    decision_add: DecisionAddModule,
    decision_ac: DecisionAcModule,

    document_add: DocumentAddModule,

    change_personal_add: ChangePersonalAddModule,
    change_collective_add: ChangeCollectiveAddModule
  }
});
