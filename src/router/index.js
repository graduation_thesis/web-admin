import Vue from "vue";
import Router from "vue-router";
// import * as auth from '../services/auth'
import Middlewares from "../middlewares/index";

import Basic from "../components/layouts/Basic";

// TRANG CHỦ ADMIN
import Home from "../views/home/index";

// ĐƠN VỊ CƠ SỞ
import User from "../views/user/index";

// ĐƠN VỊ CƠ SỞ
import Unit from "../views/unit/index";
import AddUnit from "../views/unit/add";
import EditUnit from "../views/unit/edit";

// User
// import User from '../views/user/index'
import AddUser from "../views/user/add";

// CÁN BỘ
import Staff from "../views/staff/index";
import AddStaff from "../views/staff/add";
import EditStaff from "../views/staff/edit";
import ProfileStaff from "../views/staff/profile";

// LOẠI CÁN BỘ
import TypeStaff from "../views/type_staff/index";
import AddTypeStaff from "../views/type_staff/add";
import EditTypeStaff from "../views/type_staff/edit";

// CẤP BẬC
import RankStaff from "../views/rank_staff/index";
import AddRankStaff from "../views/rank_staff/add";
import EditRankStaff from "../views/rank_staff/edit";

// CẤP BẬC
import PositionStaff from "../views/position_staff/index";
import AddPositionStaff from "../views/position_staff/add";
import EditPositionStaff from "../views/position_staff/edit";

// SỐ QUYẾT ĐỊNH
import Decision from "../views/decision/index";
import AddDecision from "../views/decision/add";
import EditDecision from "../views/decision/edit";

// VĂN BẢN
import Document from "../views/document/index";
import AddDocument from "../views/document/add";
import EditDocument from "../views/document/edit";

// CƠ QUAN QUYẾT ĐỊNH
import DecisionAgency from "../views/decision_agency/index";
import AddDecisionAgency from "../views/decision_agency/add";
import EditDecisionAgency from "../views/decision_agency/edit";

// QUÁ TRÌNH CÔNG TÁC CÁ NHÂN
import ChangePersonal from "../views/change_personal/index";
import AddChangePersonal from "../views/change_personal/add";

// QUÁ TRÌNH CÔNG TÁC TẬP THỂ
import ChangeCollective from "../views/change_collective/index";
import AddChangeCollective from "../views/change_collective/add";

// DANH HIỆU CÁ NHÂN
import PersonalTitle from "../views/personal_title/index";
import AddPersonalTitle from "../views/personal_title/add";
import EditPersonalTitle from "../views/personal_title/edit";

// DANH HIỆU TẬP THỂ
import CollectiveTitle from "../views/collective_title/index";
import AddCollectiveTitle from "../views/collective_title/add";
import EditCollectiveTitle from "../views/collective_title/edit";

// HÌNH THỨC KHEN THƯỞNG
import FormsOfReward from "../views/forms_of_reward/index";
import AddFormsOfReward from "../views/forms_of_reward/add";
import EditFormsOfReward from "../views/forms_of_reward/edit";

// HÌNH THỨC RỦI RO
import FormsRisk from "../views/forms_risk/index";
import AddFormsRisk from "../views/forms_risk/add";
import EditFormsRisk from "../views/forms_risk/edit";

// HÌNH THỨC KỶ LUẬT
import FormsOfDiscipline from "../views/forms_of_discipline/index";
import AddFormsOfDiscipline from "../views/forms_of_discipline/add";
import EditFormsOfDiscipline from "../views/forms_of_discipline/edit";

// ĐĂNG KÍ THI ĐUA CHƯA DUYỆT
import RegisterReward from "../views/register_reward/index";
import RegisterCollectiveReward from "../views/register_collective_reward/index";

// ĐĂNG KÍ THI ĐUA CÁ NHÂN ĐÃ DUYỆT
import CheckedReward from "../views/checked_reward/index";
import AddCheckedReward from "../views/checked_reward/add";
import EditCheckedReward from "../views/checked_reward/edit";

// ĐĂNG KÍ THI ĐUA ĐƠN VỊ ĐÃ DUYỆT
import CheckedCollectiveReward from "../views/checked_collective_reward/index";
import AddCheckedCollectiveReward from "../views/checked_collective_reward/add";
import EditCheckedCollectiveReward from "../views/checked_collective_reward/edit";

// KẾT QUẢ THI ĐUA CÁ NHÂN
import PersonalRewardResult from "../views/personal_reward_result/index";
import AddPersonalRewardResult from "../views/personal_reward_result/add";
import EditPersonalRewardResult from "../views/personal_reward_result/edit";

// KẾT QUẢ THI ĐUA ĐƠN VỊ
import CollectiveRewardResult from "../views/collective_reward_result/index";
import AddCollectiveRewardResult from "../views/collective_reward_result/add";
import EditCollectiveRewardResult from "../views/collective_reward_result/edit";

// THỐNG KÊ KHEN THƯỞNG CÁ NHÂN
import PersonalReward from "../views/personal_reward/index";
import AddPersonalReward from "../views/personal_reward/add";
import EditPersonalReward from "../views/personal_reward/edit";

// THỐNG KÊ RỦI RO CÁ NHÂN
import RiskDuty from "../views/risk_duty/index";
import AddRiskDuty from "../views/risk_duty/add";
import EditRiskDuty from "../views/risk_duty/edit";

// THỐNG KÊ KHEN THƯỞNG TẬP THỂ
import CollectiveReward from "../views/collective_reward/index";
import AddCollectiveReward from "../views/collective_reward/add";
import EditCollectiveReward from "../views/collective_reward/edit";

// THỐNG KÊ SỐ LIỆU
import Statistical from "../views/statistical/index";

// KỈ LUẬT CÁ NHÂN
import Discipline from "../views/discipline/index";
import AddDiscipline from "../views/discipline/add";
import EditDiscipline from "../views/discipline/edit";

// SO SÁNH KHEN THƯỞNG - KỶ LUẬT
import Compare from "../views/compare/index";

// THÀNH TÍCH CÁ NHÂN THEO ID
import PersonalAchievements from "../views/personal_achievements/index";
import AddPersonalAchievements from "../views/personal_achievements/add";
import EditPersonalAchievements from "../views/personal_achievements/edit";

// KỶ LUẬT CÁ NHÂN THEO ID
import PersonalDiscipline from "../views/personal_discipline/index";
import AddPersonalDiscipline from "../views/personal_discipline/add";
import EditPersonalDiscipline from "../views/personal_discipline/edit";

// QÚA TRÌNH CÔNG TÁC CÁ NHÂN THEO ID
import ChangeStaff from "../views/change_staff/edit";
// QÚA TRÌNH CÔNG TÁC ĐƠN VỊ CƠ SỞ THEO ID
import ChangeUnit from "../views/change_unit/edit";

// HIỂN THỊ LỖI TRANG
import Index404 from "../views/error/index404";

// TRANG LOGIN
import Login from "../views/auth/login";

// TRANG ĐƠN VỊ TRÊN CƠ SỞ
import School from "../views/school/index";
import AddSchool from "../views/school/add";
import EditSchool from "../views/school/edit";

// TRANG ĐƠN VỊ TRỰC THUỘC CƠ SỞ
import Team from "../views/team/index";
import AddTeam from "../views/team/add";
import EditTeam from "../views/team/edit";

// Ý KIẾN PHẢN HỒI
import Comment from "../views/comment/index";
import FeedBackComment from "../views/comment/feedback";

// CHỨC NĂNG NÂNG CAO (IMPORT MULTIPLE)
import Advanced from "../views/advanced/index";

Vue.use(Router);

const router = new Router({
  linkExactActiveClass: "active", // link active
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    // Trang LOGIN
    {
      path: "/login",
      name: "login.index",
      component: Login,
      meta: {
        middleware: [Middlewares.guest]
      }
      // meta: {
      //   requiresAuth: false
      // }
    },
    // Admin Pages
    {
      path: "/",
      name: "/",
      component: Basic,
      children: [
        {
          path: "/home",
          name: "home.index",
          component: Home,
          // middleware: [Middlewares.auth]
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Tài khoản
        {
          path: "/user",
          name: "user.index",
          component: User,
          // middleware: [Middlewares.guest]
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Tài khoản
        {
          path: "/user/add",
          name: "user.add",
          component: AddUser,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Số quyết định
        {
          path: "/decision",
          name: "decision.index",
          component: Decision,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm Số quyết định
        {
          path: "/decision/add",
          name: "decision.add",
          component: AddDecision,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa Số quyết định
        {
          path: "/decision/edit/:id",
          name: "decision.edit",
          component: EditDecision,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Số văn bản
        {
          path: "/document",
          name: "document.index",
          component: Document,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm văn bản
        {
          path: "/document/add",
          name: "document.add",
          component: AddDocument,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa văn bản
        {
          path: "/document/edit/:id",
          name: "document.edit",
          component: EditDocument,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Cơ quan quyết định
        {
          path: "/decisionAgency",
          name: "decisionAgency.index",
          component: DecisionAgency,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm Cơ quan quyết định
        {
          path: "/decisionAgency/add",
          name: "decisionAgency.add",
          component: AddDecisionAgency,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/decisionAgency/edit/:id",
          name: "decisionAgency.edit",
          component: EditDecisionAgency,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Quá trình công tác cá nhân
        {
          path: "/changePersonal",
          name: "changePersonal.index",
          component: ChangePersonal,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/changePersonal/add",
          name: "changePersonal.add",
          component: AddChangePersonal,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },

        // Quá trình công tác tập thể đơn vị
        {
          path: "/changeCollective",
          name: "changeCollective.index",
          component: ChangeCollective,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/changeCollective/add",
          name: "changeCollective.add",
          component: AddChangeCollective,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Danh hiệu cá nhân
        {
          path: "/personalTitle",
          name: "personalTitle.index",
          component: PersonalTitle,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/personalTitle/add",
          name: "personalTitle.add",
          component: AddPersonalTitle,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/personalTitle/edit/:id",
          name: "personalTitle.edit",
          component: EditPersonalTitle,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Danh hiệu tập thể
        {
          path: "/collectiveTitle",
          name: "collectiveTitle.index",
          component: CollectiveTitle,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/collectiveTitle/add",
          name: "collectiveTitle.add",
          component: AddCollectiveTitle,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/collectiveTitle/edit/:id",
          name: "collectiveTitle.edit",
          component: EditCollectiveTitle,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Hình thức khen thưởng
        {
          path: "/formsOfReward",
          name: "formsOfReward.index",
          component: FormsOfReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/formsOfReward/add",
          name: "formsOfReward.add",
          component: AddFormsOfReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/formsOfReward/edit/:id",
          name: "formsOfReward.edit",
          component: EditFormsOfReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Hình thức rủi ro
        {
          path: "/formsRisk",
          name: "formsRisk.index",
          component: FormsRisk,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/formsRisk/add",
          name: "formsRisk.add",
          component: AddFormsRisk,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/formsRisk/edit/:id",
          name: "formsRisk.edit",
          component: EditFormsRisk,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Hình thức kỷ luật
        {
          path: "/formsOfDiscipline",
          name: "formsOfDiscipline.index",
          component: FormsOfDiscipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/formsOfDiscipline/add",
          name: "formsOfDiscipline.add",
          component: AddFormsOfDiscipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/formsOfDiscipline/edit/:id",
          name: "formsOfDiscipline.edit",
          component: EditFormsOfDiscipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Đăng kí thi đua cá nhân chưa duyệt
        {
          path: "/registerReward",
          name: "registerReward.index",
          component: RegisterReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Đăng kí thi đua đã duyệt
        {
          path: "/checkedReward",
          name: "checkedReward.index",
          component: CheckedReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Đăng kí thi đua đơn vị chưa duyệt
        {
          path: "/registerCollectiveReward",
          name: "registerCollectiveReward.index",
          component: RegisterCollectiveReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Đăng kí thi đua đơn vị đã duyệt
        {
          path: "/checkedCollectiveReward",
          name: "checkedCollectiveReward.index",
          component: CheckedCollectiveReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm thi đua đơn vị đã duyệt
        {
          path: "/checkedCollectiveReward/add",
          name: "checkedCollectiveReward.add",
          component: AddCheckedCollectiveReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa thi đua đơn vị đã duyệt
        {
          path: "/checkedCollectiveReward/edit/:id",
          name: "checkedCollectiveReward.edit",
          component: EditCheckedCollectiveReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm thi đua cá nhân đã duyệt
        {
          path: "/checkedReward/add",
          name: "checkedReward.add",
          component: AddCheckedReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa thi đua cá nhân đã duyệt
        {
          path: "/checkedReward/edit/:id",
          name: "checkedReward.edit",
          component: EditCheckedReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Kết quả thi đua cá nhân
        {
          path: "/personalRewardResult",
          name: "personalRewardResult.index",
          component: PersonalRewardResult,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/personalRewardResult/add",
          name: "personalRewardResult.add",
          component: AddPersonalRewardResult,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/personalRewardResult/edit/:id",
          name: "personalRewardResult.edit",
          component: EditPersonalRewardResult,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Kết quả thi đua đơn vị
        {
          path: "/collectiveRewardResult",
          name: "collectiveRewardResult.index",
          component: CollectiveRewardResult,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/collectiveRewardResult/add",
          name: "collectiveRewardResult.add",
          component: AddCollectiveRewardResult,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/collectiveRewardResult/edit/:id",
          name: "collectiveRewardResult.edit",
          component: EditCollectiveRewardResult,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thống kê Khen thưởng cá nhân
        {
          path: "/personalReward",
          name: "personalReward.index",
          component: PersonalReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/personalReward/add",
          name: "personalReward.add",
          component: AddPersonalReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/personalReward/edit/:id",
          name: "personalReward.edit",
          component: EditPersonalReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thống kê rủi ro cá nhân
        {
          path: "/riskDuty",
          name: "riskDuty.index",
          component: RiskDuty,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/riskDuty/add",
          name: "riskDuty.add",
          component: AddRiskDuty,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/riskDuty/edit/:id",
          name: "riskDuty.edit",
          component: EditRiskDuty,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thống kê Khen thưởng tập thể
        {
          path: "/collectiveReward",
          name: "collectiveReward.index",
          component: CollectiveReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/collectiveReward/add",
          name: "collectiveReward.add",
          component: AddCollectiveReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/collectiveReward/edit/:id",
          name: "collectiveReward.edit",
          component: EditCollectiveReward,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thống kê số liệu
        {
          path: "/statistical",
          name: "statistical.index",
          component: Statistical,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Kỉ luật cá nhân
        {
          path: "/discipline",
          name: "discipline.index",
          component: Discipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/discipline/add",
          name: "discipline.add",
          component: AddDiscipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/discipline/edit/:id",
          name: "discipline.edit",
          component: EditDiscipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thành tích cá nhân theo ID
        {
          path: "/personalAchievements/:id",
          name: "personalAchievements.index",
          component: PersonalAchievements,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "personalAchievements/:id/add",
          name: "personalAchievements.add",
          component: AddPersonalAchievements,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "personalAchievements/:id/edit/:ai",
          name: "personalAchievements.edit",
          component: EditPersonalAchievements,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Kỷ luật cá nhân theo ID
        {
          path: "/personalDiscipline/:id",
          name: "personalDiscipline.index",
          component: PersonalDiscipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "personalDiscipline/:id/add",
          name: "personalDiscipline.add",
          component: AddPersonalDiscipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "personalDiscipline/:id/edit/:ai",
          name: "personalDiscipline.edit",
          component: EditPersonalDiscipline,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Quá trình công tác cá nhân theo ID
        {
          path: "/changeStaff/edit/:id",
          name: "changeStaff.edit",
          component: ChangeStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Quá trình công tác đơn vị cơ sở theo ID
        {
          path: "/changeUnit/edit/:id",
          name: "changeUnit.edit",
          component: ChangeUnit,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Đơn vị trên cơ sở
        {
          path: "/school",
          name: "school.index",
          component: School,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm Đơn vị trên cơ sở
        {
          path: "/school/add",
          name: "school.add",
          component: AddSchool,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa Đơn vị trên cơ sở
        {
          path: "/school/edit/:id",
          name: "school.edit",
          component: EditSchool,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // ĐƠN VỊ CƠ Sở
        {
          path: "/unit",
          name: "unit.index",
          component: Unit,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/unit/add",
          name: "unit.add",
          component: AddUnit,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa Đơn vị cơ sở
        {
          path: "/unit/edit/:id",
          name: "unit.edit",
          component: EditUnit,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Đơn vị trực thuộc cơ sở
        {
          path: "/team",
          name: "team.index",
          component: Team,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm Đơn vị trực thuộc cơ sở
        {
          path: "/team/add",
          name: "team.add",
          component: AddTeam,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa Đơn vị trực thuộc cơ sở
        {
          path: "/team/edit/:id",
          name: "team.edit",
          component: EditTeam,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // CÁN BỘ
        {
          path: "/staff",
          name: "staff.index",
          component: Staff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/staff/add",
          name: "staff.add",
          component: AddStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa cán bộ
        {
          path: "/staff/edit/:id",
          name: "staff.edit",
          component: EditStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thông tin cán bộ theo ID
        {
          path: "/staff/profile/:id",
          name: "staff.profile",
          component: ProfileStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Loại cán bộ
        {
          path: "/typeStaff",
          name: "typeStaff.index",
          component: TypeStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm Loại cán bộ
        {
          path: "/typeStaff/add",
          name: "typeStaff.add",
          component: AddTypeStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa Loại cán bộ
        {
          path: "/typeStaff/edit/:id",
          name: "typeStaff.edit",
          component: EditTypeStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Cấp bậc
        {
          path: "/rankStaff",
          name: "rankStaff.index",
          component: RankStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm Cấp bậc
        {
          path: "/rankStaff/add",
          name: "rankStaff.add",
          component: AddRankStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa Cấp bậc
        {
          path: "/rankStaff/edit/:id",
          name: "rankStaff.edit",
          component: EditRankStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Chức vụ
        {
          path: "/positionStaff",
          name: "positionStaff.index",
          component: PositionStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Thêm Chức vụ
        {
          path: "/positionStaff/add",
          name: "positionStaff.add",
          component: AddPositionStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Sửa Chức vụ
        {
          path: "/positionStaff/edit",
          name: "positionStaff.edit",
          component: EditPositionStaff,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // Ý kiến phải hồi
        {
          path: "/comment",
          name: "comment.index",
          component: Comment,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        {
          path: "/comment/feedback/:id",
          name: "comment.feedback",
          component: FeedBackComment,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // chức năng nâng cao (import multiple)
        {
          path: "/advanced",
          name: "advanced.index",
          component: Advanced,
          meta: {
            middleware: [Middlewares.auth]
            // requiresAuth: true
          }
        },
        // So sánh biểu đồ
        {
          path: "/compare",
          name: "compare.index",
          component: Compare,
          meta: {
            middleware: [Middlewares.auth]
          }
        },
        // Hiển thị lỗi trang 404 (Không tìm thấy)
        {
          path: "/index404",
          name: "index404.index",
          component: Index404
        }
      ]
    }
  ]
});

function nextCheck(context, middleware, index) {
  const nextMiddleware = middleware[index];
  if (!nextMiddleware) return context.next;
  return (...parameters) => {
    context.next(...parameters);
    const nextMidd = nextCheck(context, middleware, index + 1);

    nextMiddleware({ ...context, next: nextMidd });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];
    const ctx = {
      from,
      next,
      router,
      to
    };
    const nextMiddleware = nextCheck(ctx, middleware, 1);
    return middleware[0]({ ...ctx, next: nextMiddleware });
  }
  return next();
});

// router.beforeEach((to, from, next) => {
//   if (!auth.isLoggedIn() && to.path !== '/login') {
//     next('/login')
//   } else {
//     next()
//   }
// });

export default router;
