// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// import Axios from 'axios'
import _ from "lodash";
import VueSweetalert2 from "vue-sweetalert2";
import excel from "vue-excel-export";
import VueExcelXlsx from "vue-excel-xlsx";
import VueMoment from "vue-moment";
import Vuelidate from "vuelidate";

// Axios.defaults.baseURL = '/api/v1/' // process.env.API_URL
// Axios.defaults.baseURL = 'http://192.168.1.5:8000/api/'; // Đường dẫn đầu tiên + service api

// Axios.defaults.baseURL = 'http://localhost:8000/api/';

// Vue.prototype.$http = Axios;

Vue.config.productionTip = false;
Vue.use(VueMoment);
Vue.use(VueSweetalert2);
Vue.use(_);
Vue.use(excel);
Vue.use(VueExcelXlsx);
Vue.use(Vuelidate);

/* eslint-disable no-new */

new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App)
}).$mount("#app");
