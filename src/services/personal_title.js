import axios from "../axios";

/**
 * Tạo danh hiệu cá nhân mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("personalTitle/list");
}

function submit(data) {
  return axios.post("personalTitle/create", data);
}

function show(id) {
  return axios.get("personalTitle/show/" + id);
}

function update(id, data) {
  return axios.post("personalTitle/update/" + id, data);
}

function remove(id) {
  return axios.delete("personalTitle/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
