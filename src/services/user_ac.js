import axios from "../axios";

function getSchools() {
  return axios.get("school/list");
}

function getUnitAll() {
  return axios.get("unit/list");
}

function getUnits(id) {
  return axios.get("school/getUnits/" + id);
}

function getUnitBundle(id) {
  return axios.get("unit/bundle/" + id);
}

// end to add team

function getStaffs(id) {
  return axios.get("unit/getStaffs/" + id);
}

function getStaffBundle(id) {
  return axios.get("staff/bundle/" + id);
}

export default {
  getSchools,
  getUnitAll,
  getUnits,
  getUnitBundle,
  getStaffs,
  getStaffBundle
};
