import axios from "../axios";

/**
 * Tạo loại cán bộ mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("typeStaff/list");
}

function submit(data) {
  return axios.post("typeStaff/create", data);
}

function show(id) {
  return axios.get("typeStaff/show/" + id);
}

function update(id, data) {
  return axios.post("typeStaff/update/" + id, data);
}

function remove(id) {
  return axios.delete("typeStaff/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
