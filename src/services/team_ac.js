import axios from "../axios";

function getSchools() {
  return axios.get("school/list");
}

function getUnits(id) {
  return axios.get("school/getUnits/" + id);
}

function getUnitBundle(id) {
  return axios.get("unit/bundle/" + id);
}

export default {
  getSchools,
  getUnits,
  getUnitBundle
};
