import axios from "../axios";

/**
 * Tạo quá trình chuyển công tác đơn vị cơ sở mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function show(id) {
  return axios.get("changeUnit/show/" + id);
}

function update(id, data) {
  return axios.post("changeUnit/update/" + id, data);
}

export default {
  show,
  update
};
