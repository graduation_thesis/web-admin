import axios from "../axios";

/**
 * Tạo chức vụ cán bộ mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("positionStaff/list");
}

function submit(data) {
  return axios.post("positionStaff/create", data);
}

function show(id) {
  return axios.get("positionStaff/show/" + id);
}

function update(id, data) {
  return axios.post("positionStaff/update/" + id, data);
}

function remove(id) {
  return axios.delete("positionStaff/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
