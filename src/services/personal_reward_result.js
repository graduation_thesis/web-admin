import axios from "../axios";

/**
 * Tạo khen thưởng tập thể mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "personalRewardResult/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("personalRewardResult/create", data);
}

function show(id) {
  return axios.get("personalRewardResult/show/" + id);
}

function update(id, data) {
  return axios.post("personalRewardResult/update/" + id, data);
}

function remove(id) {
  return axios.delete("personalRewardResult/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("personalRewardResult/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "personalRewardResult/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  getSearch
};
