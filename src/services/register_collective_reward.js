import axios from "../axios";

/**
 * Tạo đăng kí thành tích đơn vị mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "registerCollectiveReward/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function getListCheckedAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "registerCollectiveReward/getListChecked?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("registerCollectiveReward/create", data);
}

function show(id) {
  return axios.get("registerCollectiveReward/show/" + id);
}

function update(id, data) {
  return axios.post("registerCollectiveReward/update/" + id, data);
}

// Duyệt đăng kí thi đua cho cán bộ
function getCheck(id, checker, data) {
  return axios.post(
    "registerCollectiveReward/checked/" + id + "/" + checker,
    data
  );
}

// Duyệt đăng kí thi đua cho tất cả cán bộ
function getCheckAll(id, checker, data) {
  return axios.post(
    "registerCollectiveReward/checked/" + id + "/" + checker,
    data
  );
}

function remove(id) {
  return axios.delete("registerCollectiveReward/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("registerCollectiveReward/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "registerCollectiveReward/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  getListCheckedAll,
  submit,
  show,
  update,
  getCheck,
  getCheckAll,
  remove,
  excelImport,
  getSearch
};
