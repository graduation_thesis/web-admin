import axios from "../axios";

/**
 * Tạo quá trình công tác cho đơn vị mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "changeCollective/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("changeCollective/create", data);
}

function show(id) {
  return axios.get("changeCollective/show/" + id);
}

function update(id, data) {
  return axios.post("changeCollective/update/" + id, data);
}

function remove(id) {
  return axios.delete("changeCollective/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("changePersonal/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "changeCollective/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  getSearch
};
