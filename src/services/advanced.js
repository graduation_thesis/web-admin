import axios from "../axios";

/**
 * Import excel multiple mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function ImportStep1(formData, config) {
  return axios.post("advanced/step1", formData, config);
}

function ImportStep2(formData, config) {
  return axios.post("advanced/step2", formData, config);
}

function ImportStep3(formData, config) {
  return axios.post("advanced/step3", formData, config);
}

export default {
  ImportStep1,
  ImportStep2,
  ImportStep3
};
