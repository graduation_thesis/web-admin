import axios from "../axios";

/**
 * Tạo văn bản mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "document/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("document/create", data);
}

function show(id) {
  return axios.get("document/show/" + id);
}

function update(id, data) {
  return axios.post("document/update/" + id, data);
}

function remove(id) {
  return axios.delete("document/remove/" + id);
}

function upload(formData, config) {
  return axios.post("document/upload/", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "document/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  upload,
  getSearch
};
