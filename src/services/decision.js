import axios from "../axios";

/**
 * Tạo số quyết định mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("decision/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "decision/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("decision/create", data);
}

function show(id) {
  return axios.get("decision/show/" + id);
}

function update(id, data) {
  return axios.post("decision/update/" + id, data);
}

function remove(id) {
  return axios.delete("decision/remove/" + id);
}

function upload(formData, config) {
  return axios.post("decision/upload/", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "decision/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  getPaginate,
  submit,
  show,
  update,
  remove,
  upload,
  getSearch
};
