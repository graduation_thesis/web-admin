import axios from "../axios";

/**
 * Tạo cơ quan quyết định mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("decisionAgency/list");
}

function submit(data) {
  return axios.post("decisionAgency/create", data);
}

function show(id) {
  return axios.get("decisionAgency/show/" + id);
}

function update(id, data) {
  return axios.post("decisionAgency/update/" + id, data);
}

function remove(id) {
  return axios.delete("decisionAgency/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
