import axios from "../axios";

/**
 * Tạo đơn vị trên cơ sở mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("school/list");
}

function submit(data) {
  return axios.post("school/create", data);
}

function show(id) {
  return axios.get("school/show/" + id);
}

function update(id, data) {
  return axios.post("school/update/" + id, data);
}

function remove(id) {
  return axios.delete("school/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
