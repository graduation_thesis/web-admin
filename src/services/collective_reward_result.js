import axios from "../axios";

/**
 * Tạo đăng kí khen thưởng đơn vị đã qua kiểm duyệt mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "collectiveRewardResult/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("collectiveRewardResult/create", data);
}

function show(id) {
  return axios.get("collectiveRewardResult/show/" + id);
}

function update(id, data) {
  return axios.post("collectiveRewardResult/update/" + id, data);
}

function remove(id) {
  return axios.delete("collectiveRewardResult/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("collectiveRewardResult/import", formData, config);
}

function excelExport() {
  return axios.get("collectiveRewardResult/export");
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "collectiveRewardResult/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  excelExport,
  getSearch
};
