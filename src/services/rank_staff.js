import axios from "../axios";

/**
 * Tạo cấp bậc mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("rankStaff/list");
}

function submit(data) {
  return axios.post("rankStaff/create", data);
}

function show(id) {
  return axios.get("rankStaff/show/" + id);
}

function update(id, data) {
  return axios.post("rankStaff/update/" + id, data);
}

function remove(id) {
  return axios.delete("rankStaff/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
