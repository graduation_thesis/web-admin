import axios from "../axios";

/**
 * Tạo quá trình chuyển công tác mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function show(id) {
  return axios.get("changeStaff/show/" + id);
}

function update(id, data) {
  return axios.post("changeStaff/update/" + id, data);
}

export default {
  show,
  update
};
