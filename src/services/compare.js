import axios from "../axios";

/**
 * Tạo kỷ luật mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getChart() {
  return axios.get("compare/chart");
}

export default {
  getChart
};
