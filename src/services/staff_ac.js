import axios from "../axios";

function getSchools() {
  return axios.get("school/list");
}

function getUnitAll() {
  return axios.get("unit/list");
}

function getUnits(id) {
  return axios.get("school/getUnits/" + id);
}

function getUnitBundle(id) {
  return axios.get("unit/bundle/" + id);
}

// end to add team

function getTeams(id) {
  return axios.get("unit/getTeams/" + id);
}

function getTeamBundle(id) {
  return axios.get("team/bundle/" + id);
}

export default {
  getSchools,
  getUnitAll,
  getUnits,
  getUnitBundle,
  getTeams,
  getTeamBundle
};
