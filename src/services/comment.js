import axios from "../axios";

/**
 * Tạo bình luận mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "comment/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("comment/create", data);
}

function show(id) {
  return axios.get("comment/show/" + id);
}

function update(id, data) {
  return axios.post("comment/update/" + id, data);
}

function remove(id) {
  return axios.delete("comment/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "comment/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  getSearch
};
