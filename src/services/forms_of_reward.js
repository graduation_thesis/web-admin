import axios from "../axios";

/**
 * Tạo hình thức mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("formsOfReward/list");
}

function submit(data) {
  return axios.post("formsOfReward/create", data);
}

function show(id) {
  return axios.get("formsOfReward/show/" + id);
}

function update(id, data) {
  return axios.post("formsOfReward/update/" + id, data);
}

function remove(id) {
  return axios.delete("formsOfReward/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
