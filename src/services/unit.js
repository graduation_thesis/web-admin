import axios from "../axios";

/**
 * Tạo đơn vị cơ sở mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("unit/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "unit/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("unit/create", data);
}

function show(id) {
  return axios.get("unit/show/" + id);
}

function update(id, data) {
  return axios.post("unit/update/" + id, data);
}

function remove(id) {
  return axios.delete("unit/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "unit/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  getPaginate,
  submit,
  show,
  update,
  remove,
  getSearch
};
