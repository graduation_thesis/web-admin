import axios from "../axios";

/**
 * Tạo quá trình công tác cá nhân mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "changePersonal/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("changePersonal/create", data);
}

function show(id) {
  return axios.get("personalReward/show/" + id);
}

function update(id, data) {
  return axios.post("changePersonal/update/" + id, data);
}

function remove(id) {
  return axios.delete("changePersonal/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("changePersonal/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "changePersonal/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  getSearch
};
