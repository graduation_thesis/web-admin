import axios from "../axios";

/**
 * Tạo hình thức kỷ luật mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("formsOfDiscipline/list");
}

function submit(data) {
  return axios.post("formsOfDiscipline/create", data);
}

function show(id) {
  return axios.get("formsOfDiscipline/show/" + id);
}

function update(id, data) {
  return axios.post("formsOfDiscipline/update/" + id, data);
}

function remove(id) {
  return axios.delete("formsOfDiscipline/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
