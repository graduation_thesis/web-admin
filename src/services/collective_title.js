import axios from "../axios";

/**
 * Tạo danh hiệu tập thể mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("collectiveTitle/list");
}

function submit(data) {
  return axios.post("collectiveTitle/create", data);
}

function show(id) {
  return axios.get("collectiveTitle/show/" + id);
}

function update(id, data) {
  return axios.post("collectiveTitle/update/" + id, data);
}

function remove(id) {
  return axios.delete("collectiveTitle/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
