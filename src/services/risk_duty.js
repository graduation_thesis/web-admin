import axios from "../axios";

/**
 * Tạo rủi ro cho cán bộ mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "riskDuty/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("riskDuty/create", data);
}

function show(id) {
  return axios.get("riskDuty/show/" + id);
}

function update(id, data) {
  return axios.post("riskDuty/update/" + id, data);
}

function remove(id) {
  return axios.delete("riskDuty/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("riskDuty/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "riskDuty/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function getChart() {
  return axios.get("riskDuty/chart");
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  getSearch,
  getChart
};
