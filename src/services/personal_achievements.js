import axios from "../axios";

/**
 * Tạo thành tích cá nhân mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(id, page, perPage, sortColumn, direction) {
  return axios.get(
    "personalAchievements/list/" +
      id +
      "?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("personalAchievements/create", data);
}

function remove(id) {
  return axios.delete("personalAchievements/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "personalAchievements/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  remove,
  getSearch
};
