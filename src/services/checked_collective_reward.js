import axios from "../axios";

/**
 * Tạo đăng kí khen thưởng đơn vị đã qua kiểm duyệt mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "checkedCollectiveReward/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("checkedCollectiveReward/create", data);
}

function show(id) {
  return axios.get("checkedCollectiveReward/show/" + id);
}

function update(id, data) {
  return axios.post("checkedCollectiveReward/update/" + id, data);
}

function remove(id) {
  return axios.delete("checkedCollectiveReward/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("checkedCollectiveReward/import", formData, config);
}

function excelExport() {
  return axios.get("checkedCollectiveReward/export");
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "checkedCollectiveReward/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  excelExport,
  getSearch
};
