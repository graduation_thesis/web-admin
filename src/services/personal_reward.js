import axios from "../axios";

/**
 * Tạo khen thưởng cá nhân mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "personalReward/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submitSingle(data) {
  return axios.post("personalReward/createSingle", data);
}

function submit(data) {
  return axios.post("personalReward/create", data);
}

function show(id) {
  return axios.get("personalReward/show/" + id);
}

function update(id, data) {
  return axios.post("personalReward/update/" + id, data);
}

function remove(id) {
  return axios.delete("personalReward/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("personalReward/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "personalReward/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function getChart() {
  return axios.get("personalReward/chart");
}

export default {
  getAll,
  submitSingle,
  submit,
  show,
  update,
  remove,
  excelImport,
  getSearch,
  getChart
};
