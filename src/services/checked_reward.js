import axios from "../axios";

/**
 * Tạo khen thưởng tập thể mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "checkedReward/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("checkedReward/create", data);
}

function show(id) {
  return axios.get("checkedReward/show/" + id);
}

function update(id, data) {
  return axios.post("checkedReward/update/" + id, data);
}

function remove(id) {
  return axios.delete("checkedReward/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("checkedReward/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "checkedReward/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  getSearch
};
