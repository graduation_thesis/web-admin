import axios from "../axios";

/**
 * Tạo hình thức rủi ro mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("formsRisk/list");
}

function submit(data) {
  return axios.post("formsRisk/create", data);
}

function show(id) {
  return axios.get("formsRisk/show/" + id);
}

function update(id, data) {
  return axios.post("formsRisk/update/" + id, data);
}

function remove(id) {
  return axios.delete("formsRisk/remove/" + id);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove
};
