import axios from "../axios";

/**
 * Tạo khen thưởng tập thể mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "collectiveReward/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("collectiveReward/create", data);
}

function show(id) {
  return axios.get("collectiveReward/show/" + id);
}

function update(id, data) {
  return axios.post("collectiveReward/update/" + id, data);
}

function remove(id) {
  return axios.delete("collectiveReward/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("collectiveReward/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "collectiveReward/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function getChart() {
  return axios.get("collectiveReward/chart");
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  getSearch,
  getChart
};
