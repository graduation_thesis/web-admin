import axios from "../axios";

function getAll() {
  return axios.get("home/list");
}

export default {
  getAll
};
