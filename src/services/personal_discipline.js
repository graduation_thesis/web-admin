import axios from "../axios";

/**
 * Tạo kỷ luật cá nhân mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(id, page, perPage, sortColumn, direction) {
  return axios.get(
    "personalDiscipline/list/" +
      id +
      "?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("personalDiscipline/create", data);
}

function remove(id) {
  return axios.delete("personalDiscipline/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "personalDiscipline/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  submit,
  remove,
  getSearch
};
