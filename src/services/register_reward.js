import axios from "../axios";

/**
 * Tạo đăng kí thành tích cá nhân mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "registerReward/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function getListCheckedAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "registerReward/getListChecked?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("registerReward/create", data);
}

function show(id) {
  return axios.get("registerReward/show/" + id);
}

function update(id, data) {
  return axios.post("registerReward/update/" + id, data);
}

// Duyệt đăng kí thi đua cho cán bộ
function getCheck(id, checker, data) {
  return axios.post("registerReward/checked/" + id + "/" + checker, data);
}

// Duyệt đăng kí thi đua cho tất cả cán bộ
function getCheckAll(id, checker, data) {
  return axios.post("registerReward/checked/" + id + "/" + checker, data);
}

function remove(id) {
  return axios.delete("registerReward/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("registerReward/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "registerReward/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  getListCheckedAll,
  submit,
  show,
  update,
  getCheck,
  getCheckAll,
  remove,
  excelImport,
  getSearch
};
