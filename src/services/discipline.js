import axios from "../axios";

/**
 * Tạo kỷ luật mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "discipline/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("discipline/create", data);
}

function show(id) {
  return axios.get("discipline/show/" + id);
}

function update(id, data) {
  return axios.post("discipline/update/" + id, data);
}

function remove(id) {
  return axios.delete("discipline/remove/" + id);
}

function excelImport(formData, config) {
  return axios.post("discipline/import", formData, config);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "discipline/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function getChart() {
  return axios.get("discipline/chart");
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  excelImport,
  getSearch,
  getChart
};
