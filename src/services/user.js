// import axios from 'axios'
import axios from "../axios";
/**
 * Tạo user mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("user/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "user/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

// function getActive () {
//   return axios.get('user/isActive')
// }

function submit(data) {
  return axios.post("user/create", data);
}

function remove(id) {
  return axios.delete("user/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "user/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

// Tình trạng tài khoản

function checkActive(id, active) {
  return axios.post("user/checkActive/" + id + "/" + active);
}

export default {
  getAll,
  getPaginate,
  // getActive,
  submit,
  remove,
  getSearch,
  checkActive
};
